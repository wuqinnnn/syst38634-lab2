package phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class SmartPhoneTest {

	// Tests for price
	@Test
	public void testGetFormattedPriceRegular() {
		SmartPhone phone = new SmartPhone("iPhone", 2000.00, 1.00);
		System.out.println(phone.getFormattedPrice());
		assertTrue("Invalid format", "$2,000".equals(phone.getFormattedPrice()));
	}
	
	@Test 
	public void testGetFormattedPriceException() {
		SmartPhone phone = new SmartPhone("iPhone", 999.0, 1.00);
		assertFalse("Invalid format", "999.0".equals(phone.getFormattedPrice()));
	}

	@Test
	public void testGetFormattedPriceBoundaryIn() {
		SmartPhone phone = new SmartPhone("iPhone", 999.0, 1.00);
		assertTrue("Invalid format", "$999".equals(phone.getFormattedPrice()));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		SmartPhone phone = new SmartPhone("iPhone", 999.0, 1.00);
		assertFalse("Invalid format", "$999.0".equals(phone.getFormattedPrice()));
	}
	
	
	// tests for version
	@Test
	public void testSetVersionRegular() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		phone.setVersion(4.0);
		assertTrue("Invalid format", phone.getVersion() == 4.0 );
	}
	
	@Test (expected = VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		phone.setVersion(5.00);
		assertFalse("Invalid format", phone.getVersion() == 5.0 );
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		phone.setVersion(4.0);
		assertTrue("Invalid format", phone.getVersion() == 4.0);
	}
	
	@Test (expected = VersionNumberException.class)
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		SmartPhone phone = new SmartPhone();
		phone.setVersion(4.1);
		assertFalse("Invalid format", phone.getVersion() == 4.1 );
	}
	

}
